# Delivery App

## Installation

``` bash
# Install dependencies
composer install

# Create file .env
cp .env.example .env
create database ex: delivery_app
update database credentials in .env file

# Generate key
php artisan key:generate

# Run migrations (tables and Seeders)
php artisan migrate --seed

# Create Server
php artisan serve

# Access Front
http://localhost:8000

# Access Admin
http://localhost:8000/admin

# Sample CSV file
/test.csv

# Live Demo
http://3.17.27.200

```
