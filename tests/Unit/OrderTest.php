<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Illuminate\Http\UploadedFile;
use App\Order;

class OrderTest extends TestCase
{
    /**
     * Test Order Tracking
     *
     * @return void
     */
    public function testOrderTracking() {
        $order = factory(Order::class)->create();


        $this->get(route('order.tracking', $order->tracking_code))
            ->assertStatus(200);
    }
}
