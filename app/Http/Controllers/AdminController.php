<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class AdminController extends Controller
{	

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index () {
    	$orders = Order::all();
	    return view('admin/import', compact('orders'));
    }

    /**
     * Import CSV
     *
     * @return Response
     */
    public function import () {
    	//validate csv file
		request()->validate([
	        'csv_file' => 'required|mimes:csv,txt'
	    ]);

		//get path
		$path = request()->file('csv_file')->getRealPath();

		$csv = [];
		$ctr = 0;
		$handle = fopen($path, 'r');

		//get csv data
		while (($row = fgetcsv($handle, 0, ',')) !== false) {
			//skip headers
		    if ($ctr != 0) {
		        $csv[] = $row;
		    }
			$ctr++;
		}

		foreach ($csv as $row) {
			//validate tracking code & delivery date column
			if (!isset($row[0]) || empty($row[0])
				|| !isset($row[1]) || empty($row[1])) {
				return;
			}

			Order::updateOrCreate(
				['tracking_code' => $row[0]],
				['delivery_date' => date('Y-m-d h:i:s', strtotime($row[1]))]
			);
		}

		return redirect('admin')->with('success', 'CSV Data succesfully imported');
    }
}
