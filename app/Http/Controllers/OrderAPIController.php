<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderAPIController extends Controller
{
	/**
     * Return Show by Tracking Code
     *
     * @return Response Json
     */
	public function showByTrackingCode($trackingCode)
	{	
		//get order by tracking code
		$order = Order::where('tracking_code', $trackingCode)->first();

		//return error message
		if (!$order) {
			$data = [
				'error' => true,
				'message' => 'Tracking Code Not found!',
			];

			return response()->json($data);
		}	

		//format delivery date 
		$order['delivery_date'] = date('M d, Y h:i A', strtotime($order['delivery_date']));

		return response()->json([
			'error' => false,
			'data' => $order
		]);
	}
}
