@extends('layouts.app')

@section('content')
     <div class="content">
            <form class="form-inline" id="tracking_form">
                <div class="form-group">
                    <input class="form-control" type="text" id="tracking_code" name="tracking_code" placeholder="Enter Tracking Code">
                    <input class="btn btn-success" type="submit" value="Submit">
                </div>
            </form>
            <br> 
            <div class="delivery-date"></div>
        </div>
    </div>

     <script src="http://code.jquery.com/jquery-3.3.1.min.js"
           integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
           crossorigin="anonymous">
    </script>

    <script>
        $(document).ready(function(){
            $('#tracking_form').on('submit', function(e) {
               e.preventDefault(); 
               var tracking_code = $('#tracking_code').val();

               $.get('/api/order/tracking/' + tracking_code, function(response, status){
                    if (response.error) {
                        $('.delivery-date').html(response.message);
                        return false;
                    }

                    $('.delivery-date').html('Delivery Date: <h3>' + response.data.delivery_date + '</h3>');
                });
            });
        });
    </script>
@endsection