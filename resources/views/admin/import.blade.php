@extends('layouts.admin')

@section('content')
    <div class="container">

        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{{ $message }}</strong>
        </div>
        @endif
        
        <div class="row">
            <form class="form-inline" method="POST" action="/admin/import" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="d-inline">
                    <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                    <button type="submit" class="btn btn-primary d-inline">
                        Upload CSV
                    </button>
                </div>
            </form>
        </div>
        <br>
        <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tracking Code</th>
                <th>Delivery Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
                <tr>
                    <td> {{ $order->id}} </td>
                    <td> {{ $order->tracking_code}} </td>
                    <td> {{ $order->delivery_date}} </td>
                </tr>
            @endforeach
        </tbody>
      </table>
    </div>
@endsection